<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NPrice extends Model
{
    protected $fillable = [
        'year', 'quarter', 'price',
    ];
}
