<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveMin extends Model
{
    protected $fillable = [
        'year', 'quarter', 'total',
    ];
}
