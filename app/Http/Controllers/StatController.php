<?php

namespace App\Http\Controllers;

use App\Income;
use App\Deficit;
use Illuminate\Http\Request;

class StatController extends Controller
{
    public function index() {
        return [
            'incomes' => Income::all(),
            'deficits' => Deficit::all(),
        ];
    }
}
