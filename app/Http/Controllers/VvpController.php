<?php

namespace App\Http\Controllers;

use App\Vvp;
use App\Curs;
use Illuminate\Http\Request;

class VvpController extends Controller
{
    public function index() 
    {
        $res = [];
        $vvp = Vvp::all();
        foreach ($vvp as $item) 
        {
            $curs = Curs::where(['year' => $item->year])->get();
            $sum = 0;
            foreach ($curs as $i) 
                $sum += floatval($i->value);
            $res[] = [
                'year' => $item->year,
                'vvp' => $item->value,
                'curs' => ($sum / 4),
            ];
        }
        return $res;
    }
}
