<?php

namespace App\Http\Controllers;

use App\LiveMin;
use App\NPrice;
use App\Curs;
use Illuminate\Http\Request;

class DiagramController extends Controller
{
    public function index() {
        return [
            'l_m' => LiveMin::all(),
            'n_p' => NPrice::all(),
            'c'   => Curs::all(),
        ];
    }
}
